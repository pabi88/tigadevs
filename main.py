#!/usr/bin/env python3

import yaml
import os
import paramiko

ssh_key_filename = os.getenv('HOME') + '/.ssh/id_rsa'

print('insert server name')
target_server = input()

with open('inventory.yaml') as f:
    data = yaml.load(f, Loader=yaml.FullLoader)
    print("IP " + data[target_server]['ip'])
    print("Bastion " + data[target_server]['bastion'])

    print('connecting....')

vm = paramiko.SSHClient()
vm.set_missing_host_key_policy(paramiko.AutoAddPolicy())
vm.connect(data[target_server]['bastion'], username='ubuntu', key_filename=ssh_key_filename)
#
vmtransport = vm.get_transport()
dest_addr = (data[target_server]['ip'], 22) #edited#
local_addr = (data[target_server]['bastion'], 22) #edited#
vmchannel = vmtransport.open_channel("direct-tcpip", dest_addr, local_addr)
#
jhost = paramiko.SSHClient()
jhost.set_missing_host_key_policy(paramiko.AutoAddPolicy())
jhost.connect(data[target_server]['ip'], username='ubuntu', key_filename=ssh_key_filename, sock=vmchannel)
#
# Place for job to be done
#
jhost.close()
vm.close()
